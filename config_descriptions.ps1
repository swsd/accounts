function Get-Staff-Description($building_code, $emp_code, $hiredate, $assignments, $description) {
    $coach_text = ""
    $coach = ""
    foreach ($assignment in $assignments) {
        # Write-Host $assignment
        if ($assignment.AssignDesc -eq "ATHLETICS") {
            $fresh = $false
            # write-host $assignment.JobTypeDesc
            if ($coach_text -eq "") {
                $fresh = $true
                $coach_text += "Coach"
            }
            if (-not $fresh) {
                $coach_text += ","
            }
            if ($assignments.JobTypeDesc -ne "") {
                $coach_text += " $($assignment.JobTypeDesc)"
            }
            
        }
    }
    if ($emp_code -eq "1MT") {
        #teachers/cert
        if ($coach_text -ne "") {
            $coach = " ($coach_text)"
        }
        if ($building_code -eq "400") {
            return "HS Certificated$coach $hiredate"
        } elseif ($building_code -eq "200") {
            return "MS Certificated$coach $hiredate"
        } elseif ($building_code -eq "100") {
            return "ES Certificated$coach $hiredate"
        } elseif ($building_code -eq "700") {
            return "SWA Certificated$coach $hiredate"
        } elseif ($building_code -eq "800") {
            return "TM Certificated$coach $hiredate"
        } elseif ($building_code -eq "900") {
            return "DSC Certificated$coach $hiredate"
        }
        return "Certificated$coach_text $hiredate"
    } elseif ($emp_code -eq "2MM") {
        #MAINTENANCE
        if ($coach_text -ne "") {
            $coach = " ($coach_text)"
        }
        if ($building_code -eq "400") {
            return "HS Maintenance$coach $hiredate"
        } elseif ($building_code -eq "200") {
            return "MS Maintenance$coach $hiredate"
        } elseif ($building_code -eq "100") {
            return "ES Maintenance$coach $hiredate"
        } elseif ($building_code -eq "700") {
            return "SWA Maintenance$coach $hiredate"
        } elseif ($building_code -eq "800") {
            return "TM Maintenance$coach $hiredate"
        } elseif ($building_code -eq "900") {
            return "DSC Maintenance$coach $hiredate"
        }
        return "Maintenance $hiredate"
    } elseif ($emp_code -eq "1S") {
        #SUB TEACHER
        if ($coach_text -ne "") {
            $coach = " ($coach_text)"
        }
        return "Sub Certificated$coach $hiredate"
    } elseif ($emp_code -eq "2S-Lock" -or $emp_code -eq "2SSec") {
        #SUB CLASSIFIED
        if ($coach_text -ne "") {
            $coach = " ($coach_text)"
        }
        return "Sub Classified$coach $hiredate"
    } elseif ($emp_code -eq "2SCDL") {
        #SUB CLASSIFIED
        if ($coach_text -ne "") {
            $coach = " ($coach_text)"
        }
        return "Sub Bus Driver$coach $hiredate"
    } elseif ($emp_code -eq "2MB") {
        #SECRETARY
        if ($coach_text -ne "") {
            $coach = " ($coach_text)"
        }
        if ($building_code -eq "400") {
            return "HS Secretary$coach $hiredate"
        } elseif ($building_code -eq "200") {
            return "MS Secretary$coach $hiredate"
        } elseif ($building_code -eq "100") {
            return "ES Secretary$coach $hiredate"
        } elseif ($building_code -eq "700") {
            return "SWA Secretary$coach $hiredate"
        } elseif ($building_code -eq "800") {
            return "TM Secretary$coach $hiredate"
        } elseif ($building_code -eq "900") {
            return "DSC Secretary$coach $hiredate"
        }
        return "Secretary $hiredate"
    } elseif ($emp_code -eq "2SP") {
        #PSE SUB
        if ($coach_text -ne "") {
            $coach = " ($coach_text)"
        }
        return "Sub Classified PSE$coach $hiredate"
    } elseif ($emp_code -eq "2MC") {
        #PARA ED/CLERK
        if ($coach_text -ne "") {
            $coach = " ($coach_text)"
        }
        if ($building_code -eq "400") {
            return "HS Para$coach $hiredate"
        } elseif ($building_code -eq "200") {
            return "MS Para$coach $hiredate"
        } elseif ($building_code -eq "100") {
            return "ES Para$coach $hiredate"
        } elseif ($building_code -eq "700") {
            return "SWA Para$coach $hiredate"
        } elseif ($building_code -eq "800") {
            return "TM Para$coach $hiredate"
        } elseif ($building_code -eq "900") {
            return "DSC Para$coach $hiredate"
        }
        return "Para$coach_text $hiredate"
    } elseif ($emp_code -eq "2C") {
        #COACH
        #$sport = $TextInfo.ToTitleCase($AssignJobTypeDesc.ToLower())
        if ($coach_building -eq "400") {
            return "HS$coach_text $hiredate"
        } elseif ($coach_building -eq "200") {
            return "MS$coach_text $hiredate"
        } elseif ($coach_building -eq "100") {
            return "ES$coach_text $hiredate"
        } elseif ($coach_building -eq "700") {
            return "SWA$coach_text $hiredate"
        } elseif ($coach_building -eq "800") {
            return "TM$coach_text $hiredate"
        } elseif ($coach_building -eq "900") {
            return "DSC$coach_text $hiredate"
        }
        return "$coach_text $hiredate"
    } elseif ($emp_code -eq "2MF") {
        #FOOD SERVICE
        if ($coach_text -ne "") {
            $coach = " ($coach_text)"
        }
        if ($building_code -eq "400") {
            return "HS Food Service $hiredate"
        } elseif ($building_code -eq "200") {
            return "MS Food Service $hiredate"
        } elseif ($building_code -eq "100") {
            return "ES Food Service $hiredate"
        } elseif ($building_code -eq "700") {
            return "SWA Food Service $hiredate"
        } elseif ($building_code -eq "800") {
            return "TM Food Service $hiredate"
        } elseif ($building_code -eq "900") {
            return "DSC Food Service $hiredate"
        }
        return "Food Service $hiredate"
    } elseif ($emp_code -eq "2MT") {
        #TRANSPORTATION
        if ($coach_text -ne "") {
            $coach = " ($coach_text)"
        }
        if ($building_code -eq "400") {
            return "HS Transportation $hiredate"
        } elseif ($building_code -eq "200") {
            return "MS Transportation $hiredate"
        } elseif ($building_code -eq "100") {
            return "ES Transportation $hiredate"
        } elseif ($building_code -eq "700") {
            return "SWA Transportation $hiredate"
        } elseif ($building_code -eq "800") {
            return "TM Transportation $hiredate"
        } elseif ($building_code -eq "900") {
            return "DSC Transportation $hiredate"
        }
        return "Transportation $hiredate"
    } elseif ($emp_code -eq "CONTRACT") {
        return "Contract Staff $description"
    } elseif ($emp_code -eq "NONSTAFF") {
        return "Nonstaff $hiredate"
    }
}