function Get-SkywardFile {
    #region Parameters
       
    [cmdletbinding()]
    Param(
            [Parameter(Mandatory=$true)] [ValidateNotNullOrEmpty()]
            [string] $RemotePath,
           
            [Parameter(Mandatory=$true)] [ValidateNotNullOrEmpty()]
            [String] $LocalPath
    )
           
    #endregion

    Begin {}

    Process {
        try
        {
            # Load WinSCP .NET assembly
            Add-Type -Path "util\WinSCPnet.dll"

            # Setup session options
            $sessionOptions = New-Object WinSCP.SessionOptions
            $sessionOptions.Protocol = [WinSCP.Protocol]::Sftp
            $sessionOptions.HostName = $Config.Skyward.HostName
            $sessionOptions.UserName = $Config.Skyward.UserName
            $sessionOptions.Password = $Config.Skyward.Password
            $sessionOptions.SshHostKeyFingerprint = $Config.Skyward.SshHostKeyFingerprint

            $session = New-Object WinSCP.Session

            try
            {
                # Connect
                $session.Open($sessionOptions)

                if ($session.FileExists($RemotePath))
                {
                    if (!(Test-Path $LocalPath))
                    {
                        Write-Log -Message (
                            "File {0} exists, local backup {1} does not" -f
                            $RemotePath, $LocalPath)
                        $download = $True
                    }


                    if ($download)
                    {
                        # Download the file and throw on any error
                        $session.GetFiles($RemotePath, $LocalPath).Check()

                        Write-Log -Message "Download done."
                    }
                }
                else
                {
                    Write-Log -Message ("File {0} does not exist yet" -f $RemotePath)
                }
            }
            finally
            {
                # Disconnect, clean up
                $session.Dispose()
            }
        }
        catch [Exception]
        {
            Write-Log -Message $_.Exception.Message
            exit 1
        }
    }

    End {}

    <#
                .SYNOPSIS
                        Writes logging information to screen and log file simultaneously.
 
                .PARAMETER RemotePath
                        The file to get.
 
                .PARAMETER LocalPath
                        The place to put the fetched file.
               
                .INPUTS
                        System.String
 
                .OUTPUTS
                        No output.
                       
        #>
}

function Get-ADParent ([string] $dn) {
     $parts = $dn -split '(?<![\\]),'
     $parts[1..$($parts.Count-1)] -join ','
}

# Set the properties we want when querying for ADUsers
$ADProperties = "employeeid","employeetype","Description","profilePath","homeDirectory","homeDrive","emailaddress","sn","middleName","givenName","cn","displayName","memberOf"

function GetUsername($username, $firstname, $lastname, $number) {
    if ($number -eq 0) {
        $User = Get-ADUser -LDAPFilter "(sAMAccountName=$($username))" -Properties $ADProperties
    } else {
        $User = Get-ADUser -LDAPFilter "(sAMAccountName=$($username + $number))" -Properties $ADProperties
    }
    If ($User -eq $Null) {
        if ($number -eq 0) {
            return "$username"
        } else {
            return "$username$number"
        }
        
    } Else {
        if ($number -eq 0) {
            $number = [int]$number + 2
        } else {
            $number = [int]$number + 1
        }
        return GetUsername "$($username)" "$($firstname)" "$($lastname)" "$($number)"
    }
}

function SendMail {
    [OutputType([System.Void])]
    param(
        [Parameter(Position=0, Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [System.Array]
        $to,
        [Parameter(Position=1, Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [System.String]
        $subject,
        [Parameter(Position=2, Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [System.String]
        $body
    )
    $emailSmtpServer = $Config.Email.SMTP.Server
    $emailSmtpServerPort = $Config.Email.SMTP.ServerPort
    $emailSmtpUser = $Config.Email.SMTP.Username
    $emailSmtpPass = $Config.Email.SMTP.Password
     
    $emailMessage = New-Object System.Net.Mail.MailMessage
    $emailMessage.From = $Config.Email.From
    foreach ($add in $to) {
        $emailMessage.To.Add( $add )
    }
    foreach ($add in $Config.Email.BCC) {
        $emailMessage.Bcc.Add( $add )
    }
    $emailMessage.ReplyTo = $Config.Email.ReplyTo
    $emailMessage.Subject = "$subject"
    $emailMessage.IsBodyHtml = $false
    $emailMessage.Body = "$body"
     
    $SMTPClient = New-Object System.Net.Mail.SmtpClient( $emailSmtpServer , $emailSmtpServerPort )
    $SMTPClient.EnableSsl = $true
    $SMTPClient.Credentials = New-Object System.Net.NetworkCredential( $emailSmtpUser , $emailSmtpPass );
     
    $SMTPClient.Send( $emailMessage )
}
