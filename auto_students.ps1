<#
.NOTES
    Name: auto_students.ps1 
    Author: Ian Turner
    Requires: PowerShell v2 or higher. 
    Version: 
    1.00   18-February-2016 - Initial Release
.SYNOPSIS
    Makes AD and Google users from Skyward data.
.DESCRIPTION
    Parses SWSDStudents.csv for user data. Looking for type:
    Adds AD and Google users if not present
    Updates AD and Google users if present with Skyward ID
.EXAMPLE
    auto_students.ps1
.LINK
    None
#>
 
Import-module ActiveDirectory
Import-module gShell
. .\util\New-SWRandomPassword.ps1
. .\util\Write-Log.ps1
. .\util\accounts_func.ps1
. .\config_descriptions.ps1
. .\config_groups.ps1
. .\config_ou.ps1

# Get the date we're running on
$stamp = Get-Date -f "yyyyMMdd"

$Config = Import-LocalizedData -FileName config.psd1

Function Log($message) {
    Write-Log -Message $message -Path (join-path -Path (Convert-Path .) -ChildPath ($Config.Log.Student -f (Get-Date -f "yyyyMMdd")))
}

# Set the properties we want when querying for ADUsers
$ADProperties = "employeeid","employeetype","Description","profilePath","homeDirectory","homeDrive","emailaddress","sn","middleName","givenName","cn","displayName","memberOf"


$stamp = Get-Date -f "yyyyMMdd"
$fileName = "student_$stamp.csv"
$remotePath = $Config.Skyward.RemoteFiles.Student
$localPath = Join-Path -Path $PSScriptRoot -ChildPath "skyward\$filename"

Get-SkywardFile -RemotePath $remotePath -LocalPath $localPath

$students = Import-CSV $localPath
$TextInfo = (Get-Culture).TextInfo

# Load all users in Google
$gusers = Get-GAUser -All -Domain $Config.Google.Domain

Function UpdateUser($user, $person) {
    # Get our current Google user by email address
    $guser = $Null
    $guser = $gusers | Where-Object { $_.PrimaryEmail -eq $user.emailaddress }
    
    # We only write to the log if we update a property
    $updated = $false
    $gupdated = $false

    # Log buffer
    $buffer = @()
    $buffer += "Updating user {0}" -f $user
    
    #Check Entity
    if ($person.entity -ne $user.employeetype) {
        $updated = $true
        $buffer += "   Entity: {0} -> {1}" -f $user.employeetype,$person.entity
        # Set-ADUser -Identity $user -Replace @{employeeType=$entity}
        $user.employeetype = $person.entity
    }

    #Check Other ID
    $id = $person.id
    if ($id -ne $user.EmployeeID) {
        $updated = $true
        $buffer += "   ID: {0} -> {1}" -f $user.EmployeeID,$person.id
        # Set-ADUser -Identity $user -Replace @{employeeId=$id}
        $user.EmployeeID = $person.id
    }
    
    #Check Description
    if ($person.entity -eq "510") {
        $description = "{0} SWA Student" -f $person.gradyear
    } else {
        $description = "{0} Student" -f $person.gradyear
    }
    if ($description -ne $user.Description) {
        $updated = $true
        $buffer += "   Description: {0} -> {1}" -f $user.Description,$description
        $user.Description = $description
    }
    
    # Write-Host "Debug: Check suspended"
    $suspended = $FALSE
    if ($guser -ne $Null -and $suspended -ne $guser.Suspended) {
        $updated = $TRUE
        $gupdated = $True
        $buffer += "   GSuspended: $($guser.Suspended) -> $suspended"
        $guser.userObject.Suspended = $suspended
    }
    
    #Check Name
    # Write-Host "Debug: Check Name"
    $updated_name = $False
    $first = $TextInfo.ToTitleCase($person.first.ToLower()).Trim()
    $middle = $TextInfo.ToTitleCase($person.middle.ToLower()).Trim()
    $last = $TextInfo.ToTitleCase($person.last.ToLower()).Trim()
    $cn = ($Config.AD.Student.CommonName -f $first, $middle, $last).Trim()
    $display = ($Config.AD.Student.DisplayName -f $first, $middle, $last).Trim()
    $gdisplay = ($Config.Google.Student.DisplayName -f $first, $middle, $last).Trim()
    
    $current_first = $user.givenName
    $current_middle = $user.middleName
    $current_last = $user.sn
    $current_display = $user.displayName
    $current_cn = $user.cn
    # Write-Host "Debug: Check givenName"
    if ($first -ne $current_first) {
        $updated = $true
        
        #Check if the first initial changed
        if ($first.substring(0,1) -ne $current_first.substring(0,1)) {
            $updated_name = $True
        }
        $buffer += "   First: $current_first -> $first"
        $user.GivenName = $first
    }
    # Write-Host "Debug: Check middleName"
    if ($middle -ne $current_middle -and -not ($middle -eq "" -and $current_middle -eq $Null)) {
        $updated = $true
        $buffer += "   Middle: $current_middle -> $middle"
        if ($middle -eq "") {
            $user.middleName = $Null
        } else {
            $user.middleName = $middle
        }
    }
    # Write-Host "Debug: Check lastName"
    if ($last -ne $current_last) {
        $updated = $true
        $updated_name = $True
        $buffer += "   Last: $current_last -> $last"
        $user.Surname = $last
    }
    # Write-Host "Debug: Check display"
    if ($display -ne $current_display) {
        $updated = $true
        $buffer += "   Display: $current_display -> $display"
        $user.DisplayName = $display
    }
    # Write-Host "Debug: Check CN"
    if ($cn -ne $current_cn) {
        $updated = $true
        $buffer += "   CN: $current_cn -> $cn (NOT SET)"
        # $user.CN = $cn
    }
    # Write-Host "Debug: Check GivenName google"
    if ($guser -ne $Null -and $first -ne $guser.userObject.name.GivenName) {
        $updated = $TRUE
        $gupdated = $True
        $buffer += "   GGivenName: {0} -> {1}" -f $guser.GivenName,$first
        $guser.userObject.name.GivenName = $first
    }
    # Write-Host "Debug: Check FamilyName google"
    if ($guser -ne $Null -and $last -ne $guser.userObject.name.FamilyName) {
        $updated = $TRUE
        $gupdated = $True
        $buffer += "   GFamilyName: {0} -> {1}" -f $guser.userObject.name.FamilyName,$last
        $guser.userObject.name.FamilyName = $last
    }
    # Write-Host "Debug: Check fullname google"
    if ($guser -ne $Null -and $gdisplay -ne $guser.userObject.name.fullname) {
        $updated = $TRUE
        $gupdated = $True
        $buffer += "   GFullName: {0} -> {1}" -f $guser.userObject.name.fullname,$gdisplay
        $guser.userObject.name.fullname = $gdisplay
    }
    
    #Check Username if name was updated
    if ($updated_name) {
        # Write-Host "Debug: Check email/username"
        # Split last name because we only want one name for shortness
        $s = $last.Split(" ,-")

        # Remove non-letters from the first part of the last name
        $last1 = $s[0] -replace '[^a-zA-Z]', ''
        
        # Get a base username without extra numbers
        $base = "$($first.substring(0,1))$last1".ToLower()
        
        # Get a username or an AD user account
        $new_username = GetUsername $base $first $last 0
        $buffer += "   Username: {0} -> {1} (SAMAccountName not update)" -f $user.SamAccountName,$new_username
        # TODO: fix setting SAMAccountName
        # $user.SamAccountName = $new_username
        # $user.UserPrincipalName = "{0}@sw.site" -f $new_username
    }
    
    #Check Profile Path
    # Write-Host "Debug: Check profile"
    # $profiledir = "\\files\Profiles$\Students\$($person.gradyear)\$($user.samaccountname)\profile"
    # $profiledir_parent = "\\files\Profiles$\Students\$($person.gradyear)\$($user.samaccountname)"
    # $profile_current = $user.profilePath
    # if ($profile_current -ne $Null) {
        # $profile_current_parent = Split-Path $profile_current -Parent
    # }
    # if ($profile_current -ne $Null -and $profile -ne $profile_current) {
        # # Write-Host "Debug: Move profile $profile_current to $profile"
        # $updated = $true
        # $buffer += "   Profile: $profile_current_parent -> $profile_parent"
        # # Write-Log -Message "   Profile: $profile_current_parent -> $profile_parent" -Path ".\logs\SWSDStudent_$stamp.log"
        # #move from current to new
        # if ($profile_current -ne $Null -and $profile_current -ne "" -and (Test-Path $profile_current)) {
            # # Write-Host "Debug: Move files from $profile_current_parent to $profile_parent"
            # Move-Item -path $profile_current_parent -destination $profile_parent
        # }
        # if (![System.IO.File]::Exists($profile_parent)) {
            # New-Item -ItemType Directory -Force -Path $profile_parent | out-null
            # $Acl = Get-Acl $profile_parent
            # # "SWSD\$username"
            # $Ar = New-Object System.Security.Accesscontrol.Filesystemaccessrule($sid,"FullControl","ContainerInherit,ObjectInherit","None","Allow")
            # $Acl.SetAccessRule($Ar)
            # Set-Acl $profile_parent $Acl
            # New-Item -ItemType Directory -Force -Path $profile | out-null
        # }
        # Set-ADUser -Identity $user -ProfilePath $profile
    # }
    
    
    #Check Home Folder
    # Write-Host "Debug: Check homedir"
    $homedir = "\\files\students\$($person.gradyear)\$($user.samaccountname)"
    $homedir_current = $user.homeDirectory
    # if ($homedir -ne $homedir_current) {
        # # Write-Host "Debug: Move homedir $homedir_current to $homedir"
        # $updated = $true
        # $buffer += "   Home: $homedir_current -> $homedir"
        # # Write-Log -Message "   Home: $homedir_current -> $homedir" -Path ".\logs\SWSDStudent_$stamp.log"
        # #move from current to new
        # if ($homedir_current -ne $Null -and $homedir_current -ne "" -and (Test-Path $homedir_current)) {
            # Move-Item -path $homedir_current -destination $homedir
        # }
        # if (![System.IO.File]::Exists($homedir)) {
            # New-Item -ItemType Directory -Force -Path $homedir | out-null
            # $Acl = Get-Acl $homedir
            # # "SWSD\$username"
            # $Ar = New-Object System.Security.Accesscontrol.Filesystemaccessrule($sid,"FullControl","ContainerInherit,ObjectInherit","None","Allow")
            # $Acl.SetAccessRule($Ar)
            # Set-Acl $homedir $Acl
        # }
        # Set-ADUser -Identity $user -HomeDirectory $homedir
    # }
    
    #Check Email
    # Write-Host "Debug: Check email"
    $email = "{0}@sw.wednet.edu" -f $user.samaccountname
    # $current_email = $user.emailaddress
    if ($email -ne $user.emailaddress) {
        $updated = $true
        $buffer += "   Email: {0} -> {1}" -f $user.emailaddress,$email
        $user.EmailAddress = $email
    }
    
    if ($updated) {
        # Save Changes to User
#        Set-ADUser -Instance $user
    }
    
    if ($guser -ne $Null -and $guser.PrimaryEmail -notlike $email) {
        $buffer += "   GUsername: $current_email -> $email"
#        Set-GAUser -UserKey $guser.PrimaryEmail -NewUserName $email
    }
    
    #Check Group
    # Write-Host "Debug: Check Group"
    $group = Get-ADGroup -Identity $person.gradyear
    $current_groups = $user.memberOf
    $cg = $current_groups | ? {$_.name -eq $person.gradyear}
    if ($current_groups -notcontains $group.DistinguishedName) {
        $updated = $true
        $buffer += "   Group: Add -> $group"
        Add-ADPrincipalGroupMembership -Identity $user -MemberOf $group
    }
    #Check for other years
    # Write-Host "Debug: Check other years"
    foreach ($cg in $current_groups) {
        if ($cg -eq $group.DistinguishedName) {
            continue
        }
        
        if ($cg -match '^CN=20[0-9]+,') {
            $updated = $TRUE
            $buffer += "   Group: Remove -> $cg"
            Remove-ADPrincipalGroupMembership -Identity $user -MemberOf $cg -Confirm:$False
        }
    }
    
    #Check Enabled
    # Write-Host "Debug: Check Enabled"
    $enabled = $TRUE
    $current_enabled = $user.enabled
    if ($enabled -ne $current_enabled) {
        $updated = $TRUE
        $buffer += "   Enabled: $current_enabled -> $enabled"
        # Write-Log -Message "   Enabled: $current_enabled -> $enabled" -Path ".\logs\SWSDStudent_$stamp.log"
        Enable-ADAccount -Identity $user
    }
    
    #Check OU
    # Write-Host "Debug: Check ou"
    $ou = $Config.AD.Student.OU -f $person.gradyear
    $current_ou = Get-ADParent $user.DistinguishedName
    if ($ou -ne $current_ou) {
        $updated = $true
        $buffer += "   OU: $current_ou -> $ou"
        # Log "   OU: $current_ou -> $ou"
        $user =  Move-ADObject -Identity $user -TargetPath $ou -PassThru
    }
    # Write-Host "Debug: Check gou"
    $gou = $Config.Google.Student.OU -f $person.gradyear
    
    if ($guser -ne $Null -and $guser.OrgUnitPath -notlike "$gou*") {
        $updated = $true
        $gupdated = $True
        $buffer += "   GOU: $($guser.OrgUnitPath) -> $gou"
        $guser.userObject.OrgUnitPath = $gou
    }
    
    if ($gupdated) {
        # Write-Host "Debug: Update Google"
        $guser.userObject.ETag = $Null
#        Set-GAUser -Userkey $guser.PrimaryEmail -UserBody $guser.userObject -Domain "sw.wednet.edu"
    }
    
    if ($guser -eq $Null) {
        # Write-Host "Debug: Create Google User"
        Log "No Google Apps user for $user, creating"
        # students more then 6 years from graudation (aka 5th graders and younger) get password as ID
        if ((get-date).month -ge 7) {
            $current_year = (get-date).year + 1
        } else {
            $current_year = (get-date).year
        }
        
        $grade_by_grad_year = (12- ([int]$person.gradyear - $current_year))
        
        if ($grade_by_grad_year -le 0) {
            # Write-Log -Message "   Skipping: Grade to low" -Path ".\logs\SWSDStudent_$stamp.log"
            return
        }
        
        if ([int]$grade_by_grad_year -le 5) {
            $Password = $person.id
            $ad_change = 0
            $g_change = $False
        } else {
            $Password = New-SWRandomPassword -InputStrings @('A-Z','a-z','0-9')
            $ad_change = 1
            $g_change = $True
        }
        
#        New-GAUser -Username $email -GivenName $first -FamilyName $last -Password $Password -IncludeInDirectory $True -Suspended $False -ChangePasswordAtNextLogin $g_change -OrgUnitPath $gou
        # TODO: Create users when none is found
    }

    
    if ($updated) {
        
        foreach ($line in $buffer) {
            Log $line
        }
    }
    
    # if ($updated_name) {
        # # Send Email
        # # Write-Host "Debug: Send update email"
        # if ($entity -eq "103") {
            # $to = @("vstembridge@sw.wednet.edu","devans@sw.wednet.edu")
        # } elseif ($entity -eq "510") {
            # $to = @("camundson@sw.wednet.edu")
        # } elseif ($entity -eq "206") {
            # $to = @("meaton@sw.wednet.edu")
        # } elseif ($entity -eq "402") {
            # $to = @("gbitts@sw.wednet.edu")
        # } else {
            # $to = @("iturner@sw.wednet.edu")
        # }
        # SendMail $to "Updated Student Account: $cn" "Hello,
            
# A student has been updated with a new username:
# $cn
# Google Apps username: $email
# Windows domain username: $new_username

# No password change has been made.

# ------
# This email was automatically generated due to changes in Skyward.
# ------
# Note: Name capitalization may not be as the family writes it. This is not fixable.
# ------

# Sincerely,
# Happy Skyward Automation Bot

# #auto_students.ps1#$($env:computername)"
    # }
}

Function CreateUser($id, $year, $entity, $username, $grade, $first, $middle, $last) {
    
    Log "Creating user $id $first, $middle, $last"
    
    $ou = $Config.AD.Student.OU -f $year
    $gou = $Config.Google.Student.OU -f $year
    $entity = $entity
    $id = $id
    $profiledir = $Config.AD.Student.ProfileDir -f $username, $year

    $homedir = $Config.AD.Student.HomeDir -f $username, $year
    $enabled = $true
    if ($entity -eq "510") {
        $description = "$year SWA Student"
    } else {
        $description = $Config.AD.Student.Description -f $year
    }
    $email = $Config.AD.Student.Email -f $username
    $first = $first.Trim()
    $middle = $middle.Trim()
    $last = $last.Trim()
    $cn = ($Config.AD.Student.CommonName -f $first, $middle, $last).Trim()
    $display = ($Config.AD.Student.DisplayName -f $first, $middle, $last).Trim()
    
    # get the current graduation year based on the month and year. Anything past July is the next year.
    $grade = $grade
    
    # students more then 6 years from graudation (aka 5th graders and younger) get password as ID
    if ((get-date).month -ge 7) {
        $current_year = (get-date).year + 1
    } else {
        $current_year = (get-date).year
    }
    
    $grade_by_grad_year = (12- ([int]$year - $current_year))
    
    if ($grade_by_grad_year -le 0) {
        # Write-Log -Message "   Skipping: Grade to low" -Path ".\logs\SWSDStudent_$stamp.log"
        return
    }
    
    if ([int]$grade_by_grad_year -le 5) {
        $Password = $id
        $ad_change = 0
        $g_change = $False
    } else {
        $Password = New-SWRandomPassword -InputStrings @('A-Z','a-z','0-9')
        $ad_change = 1
        $g_change = $True
    }

    Log "   Username: $username"
    Log "   OU: $ou"
    Log "   CN: $cn"
    Log "   Entity: $entity"
    Log "   ID: $id"
    Log "   Profile: $profiledir"
    Log "   Homedir: $homedir"
    Log "   Enabled: $enabled"
    Log "   Description: $description"
    Log "   Email: $email"
    Log "   Grade: $grade"
    Log "   Grade by Grad Year: $grade_by_grad_year"
    Log "   First: $first"
    Log "   Middle: $middle"
    Log "   Last: $last"
    Log "   Display: $display"
    Log "   Password: $Password" 
    
    $NewUser = New-ADUser -AccountPassword (ConvertTo-SecureString -AsPlainText $Password -Force) `
            -UserPrincipalName $Config.AD.Student.UserPrincipalName -f $username `
            -Enabled 1 `
            -PassThru `
            -EmployeeID "$id" `
            -ChangePasswordAtLogon $ad_change `
            -Description "$description" `
            -EmailAddress "$email" `
            -DisplayName "$display" `
            -GivenName "$first" `
            -OtherName "$middle" `
            -SurName "$last" `
            -HomeDirectory "$homedir" `
            -HomeDrive $homedir `
            -ProfilePath $profiledir `
            -Name "$cn" `
            -Path "$ou" `
            -SamAccountName "$username" `
            -OtherAttributes @{'employeetype'="$entity"}

    if ($NewUser) {
            if ($homedir -ne "") {
                New-Item -ItemType Directory -Force -Path "$homedir" | out-null
                Get-Item $homedir | Add-NTFSAccess -Account $NewUser.sid -AccessRights FullControl
            }
            if ($profiledir -ne "") {
                $profiledir_parent = Split-Path $profiledir -Parent
                New-Item -ItemType Directory -Force -Path "$profiledir_parent" | out-null
                Get-Item $profiledir_parent | Add-NTFSAccess -Account $NewUser.sid -AccessRights FullControl
            }

            Add-ADGroupMember -Identity "$year" -Members $NewUser

            # New-GAUser -Username "$email" firstname "$first" lastname "$last" password "$Password" changepassword $g_change gal on org "$gou" organization name "South Whidbey School District" title "$description" primary
            New-GAUser -Username $email -GivenName $first -FamilyName $last -Password $password -IncludeInDirectory $True -Suspended $False -ChangePasswordAtNextLogin $g_change -OrgUnitPath $gou

            if ($entity -eq "510") { # SWA 9-12
                $to = @("camundson@sw.wednet.edu")
            } elseif ($entity -eq "103" -and [int]$grade_by_grad_year -le 4) { # SWES K-4
                $to = @("vstembridge@sw.wednet.edu","devans@sw.wednet.edu")
            } elseif ($entity -eq "103" -and [int]$grade_by_grad_year -ge 5 -and [int]$grade_by_grad_year -le 6) { # SWES 5-6
                $to = @("sterhar@sw.wednet.edu")
            } elseif ($entity -eq "206") { # SWMS 7-8
                $to = @("gbitts@sw.wednet.edu")
            } elseif ($entity -eq "402") { # SWHS 9-12
                $to = @("gbitts@sw.wednet.edu")
            } else { # Catch All
                $to = @("iturner@sw.wednet.edu")
            }

            SendMail $to ($Config.Email.StudentEmailSubject -f $cn) ($Config.Email.StudentTemplate -f $cn, $email, $username, $Password, $description, $id, $entity, $grade, $ou, $env:computername)

        }
}

# Get all current student accounts
$temp = Get-ADUser -Filter * -SearchBase $Config.AD.Student.SearchBase -Properties $ADProperties
# Make student accounts an array so we can remove from it later
[System.Collections.ArrayList]$adstudents = $temp

$i = 0
foreach ($student in $students) {
    $i++

    # Set variables for convienence later
    $id = $student.id
    
    # Update our progress bar
    # Write-Progress -Activity "Checking Users..." -PercentComplete (($i / $student.Length)  * 100) -Status "$first $middle $last"
    
    # Get ADUser from current accounts by student ID if it exists
    $adstudent = $adstudents | Where-Object {$_.employeeid -eq $id}
    $user = $temp | Where-Object {$_.employeeid -eq $id}

    # Remove ADUser from our list so we can see what's left over later
    $adstudents.Remove($adstudent)

    # Check if we got a ADUser or a username, create on a username, update on an ADUser
    if ($user -eq $Null) {
        # Skip entities (599: Special ed testing)
        if ($Config.AD.Student.IgnoredEntities -contains $student.entity) {
            continue
        }

        # Set all of our names to Title Case since they start out as UPPERCASE
        $first = $TextInfo.ToTitleCase($student.first.ToLower())
        $middle = $TextInfo.ToTitleCase($student.middle.ToLower())
        $last = $TextInfo.ToTitleCase($student.last.ToLower())

        $year = $student.gradyear
        $entity = $student.entity
        $grade = $student.grade

        # Split last name because we only want one name for shortness
        $s = $last.Split(" ,-")

        # Remove non-letters from the first part of the last name
        $last1 = $s[0] -replace '[^a-zA-Z]', ''

        # Get a base username without extra numbers
        $base = "$($first.substring(0,1))$last1".ToLower()

        # Get a username
        $username = GetUsername $base $first $last 0


        # students more then 6 years from graudation (aka 5th graders and younger) get password as ID
        if ((get-date).month -ge 7) {
            $current_year = (get-date).year + 1
        } else {
            $current_year = (get-date).year
        }
        
        $grade_by_grad_year = (12- ([int]$year - $current_year))
        
        # Skip kids older than K
        if ($grade_by_grad_year -le 0) {
            continue
        }
        
        #Write-Host $username $grade_by_grad_year $first $last
        Write-Progress -Activity "Checking Users..." -PercentComplete (($i / $students.Length)  * 100) -Status "Create $first $middle $last "
        CreateUser $id $year $entity $username $grade $first $middle $last
        

    } else {
        # Write-Output "$id, $year, $entity, $base, $username, $first, $middle, $last"
        #Write-Host "Update user $username"
        Write-Progress -Activity "Checking Users..." -PercentComplete (($i / $students.Length)  * 100) -Status "Update $($student.first) $($student.middle) $($student.last)"
        
        # Write-Host "Debug: $last, $first $middle"
        # Write-Host "Debug: $username"
        UpdateUser $user $student
    }
    
}

# Done checking current active students
Write-Progress -Activity "Checking Users..." -Completed

# TODO: Set expired time
# TODO: Send warning email
# TODO: Archive/delete? home folder
# TODO: Archive/delete? profile folder?

# $adstudents | Format-Table -Property DistinguishedName, EmployeeID, Enabled
# $adstudents | Disable-ADAccount
