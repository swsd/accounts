@{
    Log = @{
        Staff = "logs\staff_{0}.log" #0: datestamp
        Student = "logs\student_{0}.log" #0: datestamp
        Emails = "logs\student_{0}.log" #0: datestamp
    }
    Email = @{
        From = 'Accounts Bot <no_reply@sw.wednet.edu>'
        SMTP = @{
            Server = "smtp-relay.gmail.com"
            ServerPort = "587"
            Username = ""
            Password = ""
        }
        BCC = @(
            ""
        )
        ReplyTo = ""
        StudentEmailSubject = "New Student Account: {0}"  #0: Common Name
        StudentTemplate = "Hello,

A new student has been added:
{0}
Google Apps username: {1}
Windows domain username: {2}
Temporary password for use with both systems: {3}

If the password above is the student ID, no password change is forced on first logon.

Description: {4}
Student ID: {5}
Entity: {6}
Grade: {7}
Account Path: {8}

------
This email was automatically generated due to changes in Skyward.
------
Note: Name capitalization may not be as the family writes it. This is not fixable.
------

Sincerely,
Happy Skyward Automation Bot

#auto_students.ps1#{9}"
        StaffNotify = @("")
        StaffEmailSubject = "New Employee Account: {0}"  #0: Common Name
        StaffTemplate = "Hello,

A new employee has been added:
{0}
Google Apps username: {1}
Windows domain username: {2}
Temporary password for use with both systems: {3}

Description: {4}
Employee ID: {5}
Employee Type: {6}
Account Path: {7}

------
This email was automatically generated due to changes in Skyward.
------

Sincerely,
Happy Skyward Automation Bot

#auto_staff.ps1#{8}"
    }
    Skyward = @{
        HostName = "152.157.6.87"
        UserName = ""
        Password = ''
        SshHostKeyFingerprint = ""
        RemoteFiles = @{
            SchoolEmails = "/Custom/skywardemails.csv"
            Student = "/Custom/SWSDStudent.csv"
            Staff = "/Custom/SWSDStaff.csv"
        }
    }
    AD = @{
        Staff = @{
            SearchBase = "OU=Staff,OU=SWSD,DC=sw,DC=site"
            ProfileDir = "\\files\Profiles$\Staff\{0}\profile" #0: username
            HomeDir = "\\files\StaffHome\{0}" #0: username
            HomeDrive = "U:"
            AdminDescription = "{0} {1}" #0: Employee Description, #1: Hire Date
            Email = "{0}@sw.wednet.edu" #0: username
            UserPrincipalName = "{0}@sw.site" #0: username
            CommonName  = "{2}, {0} {1}" #0: first, 1: middle, 2: last
            DisplayName = "{2}, {0}" #0: first, 1: middle, 2: last

        }
        Student = @{
            SearchBase = "OU=Students,OU=SWSD,DC=sw,DC=site"
            ProfileDir = "" #\\files\Profiles$\Students\{0}" #0: username, #1: graduation year
            HomeDir = "\\files\Students\{1}\{0}" #0: username, #1: graduation year
            HomeDrive = "U:"
            Email = "{0}@sw.wednet.edu" #0: username, #1: graduation year
            UserPrincipalName = "{0}@sw.site" #0: username, #1: graduation year
            CommonName  = "{2}, {0} {1}" #0: first, 1: middle, 2: last
            DisplayName = "{2}, {0}" #0: first, 1: middle, 2: last
            OU = "OU=Class{0},OU=Students,OU=SWSD,DC=sw,DC=site" #0: graduation year
            Description = "{0} Student"
            DescriptionOverride = @{
                "510" = "{0} SWA Student"
            }
            IgnoredEntities = @("599")
        }
    }

    Google = @{
        Domain = "sw.wednet.edu"
        Staff = @{
            SearchBase = "/Staff*"
            SearchExcludes = @("/Staff/Board", "/Staff/Tech/Admin*")
        }
        Student = @{
            OU = "/Students/Class{0}" #0: graduation year
            DisplayName = "{0} {2} (Student)" #0: first, 1: middle, 2: last
        }
    }
}