function Get-Staff-ADOU($building_code, $emp_code, $enabled) {
    if ($emp_code -eq "2S" -or $emp_code -eq "2SP" -or $emp_code -eq "2R" -or $emp_code -eq "2T" -or -not $enabled) {
        return "OU=Former Employees,OU=SWSD,DC=sw,DC=site"
    } elseif ($emp_code -eq "COB" -or $emp_code -eq "2CD") {
        return "OU=Former Employees,OU=SWSD,DC=sw,DC=site"
    } elseif ($emp_code -eq "CONTRACT") {
        return "OU=Contract Staff,OU=Staff,OU=SWSD,DC=sw,DC=site"
    } elseif ($emp_code -eq "2SCDL" -or $emp_code -eq "2S-Lock" -or $emp_code -eq "2SSec") {
        return "OU=Classified,OU=Substitutes,OU=Staff,OU=SWSD,DC=sw,DC=site"
    } elseif ($building_code -eq "12" -or $emp_code -eq "1S") {
        return "OU=Certificated,OU=Substitutes,OU=Staff,OU=SWSD,DC=sw,DC=site"
    } elseif ($building_code -eq "400") {
        return "OU=HS,OU=Staff,OU=SWSD,DC=sw,DC=site"
    } elseif ($building_code -eq "200") {
        return "OU=MS,OU=Staff,OU=SWSD,DC=sw,DC=site"
    } elseif ($building_code -eq "100") {
        return "OU=ES,OU=Staff,OU=SWSD,DC=sw,DC=site"
    } elseif ($building_code -eq "700") {
        return "OU=SWA,OU=Staff,OU=SWSD,DC=sw,DC=site"
    } elseif ($building_code -eq "800") {
        return "OU=TM,OU=Staff,OU=SWSD,DC=sw,DC=site"
    } elseif ($building_code -eq "900") {
        return "OU=DO,OU=Staff,OU=SWSD,DC=sw,DC=site"
    } elseif ($building_code -eq "08") {
        return "OU=Tech,OU=Staff,OU=SWSD,DC=sw,DC=site"
    } elseif ($building_code -eq "00") {
        if ($emp_code -eq "2C") {
            return "OU=Non-Staff Coaches,OU=Staff,OU=SWSD,DC=sw,DC=site"
        } else {
            return "OU=Staff,OU=SWSD,DC=sw,DC=site"
        }
    } else {
        return "OU=Staff,OU=SWSD,DC=sw,DC=site"
    }
}

function Get-Staff-GOU($building_code, $emp_code, $enabled) {
    if ($emp_code -eq "2S" -or $emp_code -eq "2SP" -or $emp_code -eq "2R" -or $emp_code -eq "2T" -or -not $enabled) {
        return "/Former employees"
    } elseif ($emp_code -eq "COB" -or $emp_code -eq "2CD") {
        return "/Former employees"
    } elseif ($emp_code -eq "CONTRACT") {
        return "/Staff/Contract Staff"
    } elseif ($emp_code -eq "2SCDL" -or $emp_code -eq "2S-Lock" -or $emp_code -eq "2SSec") {
        return "/Staff/Substitutes/Classified"
    } elseif ($building_code -eq "12" -or $emp_code -eq "1S") {
        return "/Staff/Substitutes/Certificated"
    } elseif ($building_code -eq "400") {
        return "/Staff/HS"
    } elseif ($building_code -eq "200") {
        return "/Staff/MS"
    } elseif ($building_code -eq "100") {
        return "/Staff/ES"
    } elseif ($building_code -eq "700") {
        return "/Staff/SWA"
    } elseif ($building_code -eq "800") {
        return "/Staff/TM"
    } elseif ($building_code -eq "900") {
        return "/Staff/DO"
    } elseif ($building_code -eq "08") {
        return "/Staff/Tech"
    } elseif ($building_code -eq "00") {
        if ($emp_code -eq "2C") {
            return "/Non-Staff Coaches"
        } else {
            return "/Staff"
        }
    } else {
        return "/Staff"
    }
}