<#
.NOTES
    Name: auto_staff.ps1 
    Author: Ian Turner
    Requires: PowerShell v2 or higher. 
    Version: 
    1.00   18-February-2016 - Initial Release
.SYNOPSIS
    Makes AD and Google users from Skyward data.
.DESCRIPTION
    Parses SWSDStudents.csv for user data. Looking for type:
    Adds AD and Google users if not present
    Updates AD and Google users if present with Skyward ID
.EXAMPLE
    auto_staff.ps1
.LINK
    None
#>
 
Import-module ActiveDirectory
Import-module gShell
. .\util\New-SWRandomPassword.ps1
. .\util\Write-Log.ps1
. .\util\accounts_func.ps1
. .\config_descriptions.ps1
. .\config_groups.ps1
. .\config_ou.ps1

$Config = Import-LocalizedData -FileName config.psd1


$ADProperties = "employeeid","employeetype","Description","profilePath","homeDirectory","homeDrive","emailaddress","sn","middleName","givenName","cn","displayName","adminDescription","memberOf"

$stamp = Get-Date -f "yyyyMMdd"
$fileName = "staff_$stamp.csv"
$remotePath = $Config.Skyward.RemoteFiles.Staff
$localPath = Join-Path -Path $PSScriptRoot -ChildPath "skyward\$filename"
Get-SkywardFile -RemotePath $remotePath -LocalPath $localPath

$people = Import-CSV $localPath
$supplemental = Import-CSV (Join-Path -Path $PSScriptRoot -ChildPath "staff_supplemental.csv")
$people = $people + $supplemental
$aup_check = Import-CSV (Join-Path -Path $PSScriptRoot -ChildPath "staff_aup_check.csv")
$TextInfo = (Get-Culture).TextInfo

$groups = @{
    AD = @{}
    Google = @{}
}


Function UpdateUser($user, $person, $enabled) {
    # Get our current Google user by email address
    $guser = $Null
    $guser = $gusers | Where-Object { $_.PrimaryEmail -eq $user.emailaddress }

    # We only write to the log if we update a property
    $updated = $false
    $gupdated = $false
    
    # Log buffer
    $buffer = @()
    $buffer += "Updating user $user"
    
    #Check Emp Type Code
    if ($person.emptype -ne $user.employeetype) {
        $updated = $true
        $buffer += "   Employee Type: {0} -> {1}" -f $user.employeetype,$person.emptype
        $user.employeetype = $person.emptype
    }
    #Check ID
    $id = $person.id
    if ($person.id -ne $user.employeeid) {
        $updated = $true
        $buffer += "   ID: {0} -> {1}" -f $user.employeeid,$person.id
        $user.EmployeeID = $person.id
    }
    
    #Check Profile Path
    $profiledir = $Config.AD.Staff.ProfileDir -f $user.samaccountname
    $profile_parent = Split-Path $profiledir -Parent
    $profile_current = $user.profilePath
    if ($profiledir -ne $profile_current -and $profile_current -ne $Null) {
#        $profile_current_parent = Split-Path $profile_current -Parent
#        $updated = $true
#        $buffer += "   Profile: $profile_current -> $profiledir"
#        move from current to new
#        if (Test-Path $profile_current_parent) {
#            Move-Item -path $profile_current_parent -destination $profile_parent
#        }
#        if (Test-Path -path $profile_parent) {
#            New-Item -ItemType Directory -Force -Path $profile_parent | out-null
#            Get-Item $profile_parent | Add-NTFSAccess -Account $NewUser -AccessRights FullControl
#        }
#        Set-ADUser -Identity $user -ProfilePath $profile
    }
    
    
    # Check Home Folder
    $homedir = $Config.AD.Staff.HomeDir -f $user.samaccountname
    $homedir_current = $user.homeDirectory
    if ($homedir -ne $homedir_current) {
#        $updated = $true
#        $buffer += "   Home: $homedir_current -> $homedir"
#        move from current to new
#        if (Test-Path $homedir_current) {
#            Move-Item -path $homedir_current -destination $homedir
#        }
#        if (Test-Path $homedir) {
#            New-Item -ItemType Directory -Force -Path "$homedir" | out-null
#            Get-Item $homedir | Add-NTFSAccess -Account $NewUser -AccessRights FullControl
#        }
#        Set-ADUser -Identity $user -HomeDirectory $homedir
    }
    
    #Check Enabled
    if ($person.emptype -eq "2CD" -or $person.emptype -eq "COB" -or $person.emptype -eq "1R") {
        $enabled = $false
    }
    
    $current_enabled = $user.enabled
    if ($enabled -ne $user.enabled) {
        $updated = $true
        $buffer += "   Enabled: {0} -> {1}" -f $user.enabled,$enabled
        if ($enabled) {
            Enable-ADAccount -Identity $user
        } else {
            Disable-ADAccount -Identity $user
        }
    }
    $suspended = -not $enabled
    if ($guser -ne $Null -and $suspended -ne $guser.Suspended) {
        $updated = $TRUE
        $gupdated = $True
        $buffer += "   GSuspended: $($guser.Suspended) -> $suspended"
        $guser.userObject.Suspended = $suspended
    }
    
    #Check Description
    $description = "{0} {1}" -f $person.description,$person.hiredate  #$description $hiredate
    if ($description -ne $user.adminDescription) {
        $updated = $true
        $buffer += "   Description: {0} -> {1}" -f $user.adminDescription,$description
        $user.adminDescription = $description
    }
    
    $view_desc = Get-Staff-Description $person.building $person.emptype $person.hiredate $person.assignments $person.
    if ($emptype -eq "2C" -and $view_desc -ne $user.description) {
        $updated = $true
        $buffer += "   View Description: {0} -> {1}" -f $user.Description,$view_desc
        $user.Description = $view_desc
    }
    
    #Check Group
    # Write-Host "Debug: Check Group"
    $group = Get-Staff-Group $person.building $person.emptype $person.assignments
    $ggroup = Get-Staff-GGroup $person.building $person.emptype $person.assignments
    $current_groups = $user.memberOf
    $newg = $false
    foreach ($g in $group) {
        if (-not $groups.AD.ContainsKey($g)) {
            $groups.AD[$g] = Get-ADGroup -Identity $g
        }
        # write-host $current_groups
        if ($current_groups -notcontains $groups.AD[$g].DistinguishedName) {
            $updated = $true
            $buffer += "   Group: Add -> $g"
            Add-ADPrincipalGroupMembership -Identity $user -MemberOf $groups.AD[$g]
        }
    }

    foreach ($g in $ggroup) {
        if (-not $groups.Google.ContainsKey($g)) {
            $groups.Google[$g] = Get-GAGroupMember -GroupName $g
        }
        if ($groups.Google[$g].Email -notcontains $user.emailaddress) {
            $updated = $true
            $buffer += "   GGroup: Add -> $g"
            Add-GAGroupMember -GroupName $g -Username $user.emailaddress
        }
    }
    
    #Check Name
    $updated_name = $False
    $first = $person.first
    $middle = $person.middle
    $last = $person.last
    if ($first.length -le 2) {
        $first = "$first $middle".Trim()
        $middle = ""
    }
    $cn = ($Config.AD.Staff.CommonName -f $first, $middle, $last).Trim()
    $display = ($Config.AD.Staff.DisplayName -f $first, $middle, $last).Trim()
    
    $current_first = $user.givenName
    $current_middle = $user.middleName
    $current_last = $user.sn
    $current_display = $user.displayName
    $current_cn = $user.cn
    if ($first -ne $current_first) {
        $updated = $true
        #Check if the first initial changed
        if ($first.substring(0,1) -ne $current_first.substring(0,1)) {
            $updated_name = $True
        }
        $buffer += "   First: $current_first -> $first"
        $user.GivenName = $first
    }
    if ($middle -ne $current_middle -and -not ($middle -eq "" -and $current_middle -eq $Null)) {
        $updated = $true
        $buffer += "   Middle: $current_middle -> $middle"
        if ($middle -eq "") {
            $user.middleName = $Null
        } else {
            $user.middleName = $middle
        }
    }
    if ($last -ne $current_last) {
        $updated = $true
        $updated_name = $True
        $buffer += "   Last: $current_last -> $last"
        $user.SurName = $last
    }
    if ($display -ne $current_display) {
        $updated = $true
        $buffer += "   Display: $current_display -> $display"
        $user.DisplayName = $display
    }
    
    #Check Email
    # Write-Host "Debug: Check email"
    $email = $Config.AD.Staff.Email -f $user.samaccountname
    if ($email -ne $user.emailaddress) {
        $updated = $true
        $buffer += "   Email: {0} -> {1}" -f $user.emailaddress,$email
        $user.EmailAddress = $email
    }
    
    if ($updated) {
        # Save Changes to User
        $user = Set-ADUser -Instance $user -PassThru
    }
    
    if ($cn -ne $current_cn) {
        $updated = $true
        $buffer += "   CN: $current_cn -> $cn (NOT SET)"
        
        #Change it
        # Rename-ADObject -Identity $user -NewName "$cn"
    }
    
    #Check OU
    $ou = Get-Staff-ADOU $person.building $person.emptype $enabled
    $current_ou = Get-ADParent $user.DistinguishedName
    if ($ou -ne $current_ou) {
        $updated = $true
        $buffer += "   OU: $current_ou -> $ou"
        # Write-Log -Message "   OU: $current_ou -> $ou" -Path ".\logs\staff_$stamp.log"
        $user =  Move-ADObject -Identity $user -TargetPath $ou -PassThru
    }
    $gou = Get-Staff-GOU $person.building $person.emptype $enabled
    $gou = "$gou"
    if ($guser -ne $Null -and $guser.OrgUnitPath -notlike "$gou*") {
        $updated = $true
        $gupdated = $True
        $buffer += "   GOU: $($guser.OrgUnitPath) -> $gou"
        $guser.userObject.OrgUnitPath = $gou
    }
    
    if ($gupdated) {
        # Write-Host "Debug: Update Google"
        $guser.userObject.ETag = $Null
        Set-GAUser -Userkey $guser.PrimaryEmail -UserBody $guser.userObject -Domain $Config.Google.Domain
    }
    
    if ($guser -eq $Null) {
        # Write-Host "Debug: Create Google User"
        Write-Log -Message "No Google Apps user for $user, creating" -Path ".\logs\staff_$stamp.log"
    }
    
    if ($updated) {
        foreach ($line in $buffer) {
            Write-Log -Message $line -Path ".\logs\staff_$stamp.log"
        }
    }
}

Function CreateUser($person, $username) {
    
    Write-Log -Message ("Creating user {0} {1}, {2}, {3}" -f $person.id, $person.first, $person.middle, $person.last) -Path ".\logs\staff_$stamp.log"
    
    $ou = Get-Staff-ADOU $person.building $person.emptype $true
    $gou = Get-Staff-GOU $person.building $person.emptype $true
    $emptype = $person.emptype
    $building = $person.building
    $id = $person.id
    $profiledir = $Config.AD.Staff.ProfileDir -f $username.ToLower()
    $homedir = $Config.AD.Staff.HomeDir -f $username.ToLower()
    $enabled = $true
    $description_admin = $Config.AD.Staff.AdminDescription -f $person.description, $person.hiredate
    $email = $Config.AD.Staff.Email -f $username
    $first = $person.first.Trim()
    $middle = $person.middle.Trim()
    $last = $person.last.Trim()
    $cn = ($Config.AD.Staff.CommonName -f $first, $middle, $last).Trim()
    $display = ($Config.AD.Staff.DisplayName -f $first, $middle, $last).Trim()
    
    $description = Get-Staff-Description $building $emptype $person.hiredate $person.assignments $person.description
    
    $group = Get-Staff-Group $building $emptype $person.assignments
    $ggroup = Get-Staff-GGroup $building $emptype $person.assignments
    
    $Password = New-SWRandomPassword -InputStrings @('A-Z','a-z','0-9')

    Write-Log -Message "   Username: $username" -Path ".\logs\staff_$stamp.log"
    Write-Log -Message "   OU: $ou" -Path ".\logs\staff_$stamp.log"
    Write-Log -Message "   CN: $cn" -Path ".\logs\staff_$stamp.log"
    Write-Log -Message "   Employee Type: $emptype" -Path ".\logs\staff_$stamp.log"
    Write-Log -Message "   Building: $building" -Path ".\logs\staff_$stamp.log"
    Write-Log -Message "   ID: $id" -Path ".\logs\staff_$stamp.log"
    Write-Log -Message "   Profile: $profiledir" -Path ".\logs\staff_$stamp.log"
    Write-Log -Message "   Homedir: $homedir" -Path ".\logs\staff_$stamp.log"
    Write-Log -Message "   Enabled: $enabled" -Path ".\logs\staff_$stamp.log"
    Write-Log -Message "   Admin Description: $description_admin" -Path ".\logs\staff_$stamp.log"
    Write-Log -Message "   View Description: $description" -Path ".\logs\staff_$stamp.log"
    Write-Log -Message "   Groups: $group" -Path ".\logs\staff_$stamp.log"
    Write-Log -Message "   GGroups: $ggroup" -Path ".\logs\staff_$stamp.log"
    Write-Log -Message ("   Email: {0}" -f $person.email) -Path ".\logs\staff_$stamp.log"
    Write-Log -Message "   First: $first" -Path ".\logs\staff_$stamp.log"
    Write-Log -Message "   Middle: $middle" -Path ".\logs\staff_$stamp.log"
    Write-Log -Message "   Last: $last" -Path ".\logs\staff_$stamp.log"
    Write-Log -Message "   Display: $display" -Path ".\logs\staff_$stamp.log"
    Write-Log -Message "   Password: $Password" -Path ".\logs\staff_$stamp.log"
    
    $NewUser = New-ADUser -AccountPassword (ConvertTo-SecureString -AsPlainText $Password -Force) `
            -UserPrincipalName ($Config.AD.Staff.UserPrincipalName -f $username) `
            -Enabled 1 `
            -PassThru `
            -EmployeeID "$id" `
            -ChangePasswordAtLogon 1 `
            -EmailAddress $email `
            -DisplayName "$display" `
            -GivenName "$first" `
            -OtherName "$middle" `
            -SurName "$last" `
            -Name "$cn" `
            -Path "$ou" `
            -SamAccountName "$username" `
            -Description "$description" `
            -OtherAttributes @{'employeetype'="$emptype"; 'adminDescription'="$description_admin"} `
            -HomeDirectory "$homedir" `
            -HomeDrive $Config.AD.Staff.HomeDrive `
            -ProfilePath "$profiledir"
    if ($NewUser) {
        if ($homedir -ne "") {
            New-Item -ItemType Directory -Force -Path "$homedir" | out-null
            Get-Item $homedir | Add-NTFSAccess -Account $NewUser.sid -AccessRights FullControl
        }
        if ($profiledir -ne "") {
            $profiledir_parent = Split-Path $profiledir -Parent
            New-Item -ItemType Directory -Force -Path "$profiledir_parent" | out-null
            Get-Item $profiledir_parent | Add-NTFSAccess -Account $NewUser.sid -AccessRights FullControl
        }

        foreach ($g in $group) {
            Add-ADPrincipalGroupMembership -Identity $NewUser -MemberOf $g
        }

        New-GAUser -Username $email -GivenName $first -FamilyName $last -Password $Password -IncludeInDirectory $True -Suspended $False -ChangePasswordAtNextLogin $True -OrgUnitPath $gou
        foreach ($g in $ggroup) {
            Add-GAGroupMember -GroupName $g -Username $email
        }

        SendMail $Config.Email.StaffNotify ($Config.Email.StaffEmailSubject -f $cn) ($Config.Email.StaffTemplate -f $cn, $email, $username, $Password, $view_desc, $id, $emptype, $ou, $env:computername)

    }
}

$staff = @{}

foreach ($person in $people) {
    $first = $TextInfo.ToTitleCase($person.first.ToLower()).Trim()
    $middle = $TextInfo.ToTitleCase($person.middle.ToLower()).Trim()
    $last = $TextInfo.ToTitleCase($person.last.ToLower()).Trim()
    
    $id = ($person.id).Trim()
    
    $emptype = ($person.empcode).Trim()
    $building = ($person.building).Trim()
    $description = ($person.empdesc).Trim()
    $hiredate = ($person.HireDate).Trim()
    $termdate = ($person.TermDate).Trim()
    $rehiredate = ($person.RehireDate).Trim()
    
    if ($aup_check.id -contains $id) {
        $crow = $aup_check[$aup_check.id.indexOf($id)]
        if ($crow.last -ne "") {
            $last = $crow.last
        }
        if ($crow.first -ne "") {
            $first = $crow.first
        }
        if ($crow.emptype -ne "") {
            $emptype = $crow.emptype
        }
    }
    if ($hiredate -ne "") {
        $hdate = Get-Date -Date $hiredate -EA SilentlyContinue
    } else {
        $hdate = $Null
    }
    if ($termdate -ne "") {
        $tdate = Get-Date -Date $termdate -EA SilentlyContinue
    } else {
        $tdate = $Null
    }
    if ($rehiredate -ne "") {
        $rdate = Get-Date -Date $rehiredate -EA SilentlyContinue
    } else {
        $rdate = $Null
    }
    
    if ($staff.ContainsKey($id)) {
        $assignments = $staff.$id.'assignments'
        # $assignments
        # $assignments | ForEach-Object { $_ }
        # Write-Host
        $assignments += @{'AssignDesc'=($person."Position Assignment Desc").Trim();
                          'JobTypeDesc'=$TextInfo.ToTitleCase($person."Position Job Type Desc".ToLower()).Trim();
                          'BuildingDesc'=($person."Position Building Desc").Trim();
                          'BuildingCode'=($person."Position Building Code").Trim();
                          'JobTypeCode'=($person."Position Job Type Code").Trim()}
                          
        $staff.$id.Set_Item('assignments', $assignments)
        
    } else {
        $staff.Add($id, @{'id'=$id;
                          'first'=$first;
                          'middle'=$middle;
                          'last'=$last;
                          'emptype'=$emptype;
                          'building'=$building;
                          'description'=$description;
                          'hiredate'=$hiredate;
                          'termdate'=$termdate;
                          'rehiredate'=$rehiredate;
                          'hdate'=$hdate;
                          'tdate'=$tdate;
                          'rdate'=$rdate;
                          'assignments'=@(
                                    @{'AssignDesc'=($person."Position Assignment Desc").Trim();
                                      'JobTypeDesc'=($person."Position Job Type Desc").Trim();
                                      'BuildingDesc'=($person."Position Building Desc").Trim();
                                      'BuildingCode'=($person."Position Building Code").Trim();
                                      'JobTypeCode'=($person."Position Job Type Code").Trim()}
                                  )
                          })
    }
    
    
}

# Load all users in Google
$gusers = Get-GAUser -All -Domain $Config.Google.Domain

# Get all current student accounts
$temp = Get-ADUser -Filter * -SearchBase $Config.AD.Staff.SearchBase -Properties $ADProperties
# Make student accounts an array so we can remove from it later
[System.Collections.ArrayList]$adstaff = $temp

#TODO: Pull out search excludes to config
[System.Collections.ArrayList]$gstaff = $gusers | Where-Object { $_.OrgUnitPath -like $Config.Google.Staff.SearchBase } | Where-Object { $_.OrgUnitPath -notlike "/Staff/Board" } | Where-Object { $_.OrgUnitPath -notlike "/Staff/Tech/Admin*" }

foreach ($item in $staff.GetEnumerator()) {
    $person = $item.Value
    $id = $person.id
    $view_desc = Get-Staff-Description $person.building $person.emptype $person.hiredate $person.assignments $person.description
    
    # Get ADUser from current accounts by student ID if it exists
    $adperson = $adstaff | Where-Object {$_.employeeid -eq $id}
    $guser = $gstaff | Where-Object { $_.PrimaryEmail -eq $adperson.emailaddress }

    # Remove ADUser from our list so we can see what's left over later
    $adstaff.Remove($adperson)
    $gstaff.Remove($guser)
    
    $now = Get-Date
    if ($person.emptype -ne "" -and $person.building -ne "") {
        $s = $person.last.Split(" ,-")
        $last1 = $s[0] -replace '[^a-zA-Z]', ''
        
        $base = "$($person.first.substring(0,1))$last1".ToLower()
        
        $User = Get-ADUser -Filter {employeeid -like $id} -Properties $ADProperties
        
        if ($User -eq $Null) {
            $username = GetUsername $base $person.first $person.last 0
            # # Write-Output "$($person.id), $($person.emptype), $base, $($person.first), $($person.middle), $($person.last)"
            if (($person.emptype -ne "2S" -and $person.emptype -ne "2CD" -and $person.emptype -ne "2SP" -and $person.emptype -ne "COB" -and $person.emptype -ne "2 Sum" -and $person.emptype -ne "1R") -or $aup_check.id -contains $person.id) {
                
                #check if terminated
                if ($person.tdate -ne $Null) {
                    #terminated in the past
                    
                    #check if rehired
                    if ($person.rdate -ne $Null -and $person.rdate -gt $person.tdate) {
                        #rehired after termination
                        #do nothing continue to create user/AUP check
                        Write-Log -Message "Create rehired user $($person.id) $($person.emptype) $($person.first), $($person.middle), $($person.last) ($($person.rdate))" -Path ".\logs\staff_$stamp.log"
                    } else {
                        #terminated
                        Write-Log -Message "Skipping user $($person.id) $($person.emptype) $($person.first), $($person.middle), $($person.last) (Terminated $($person.tdate))" -Path ".\logs\staff_$stamp.log"
                        #skip user
                        continue
                    }
                }
                
                #check if user has AUP on file by looking in staffcreate.csv
                if ($aup_check.id -contains $person.id) {
                    # $item
                    CreateUser $person $username
                } else {
                    Write-Log -Message "Skipping user $($person.id) $($person.emptype) $($person.first), $($person.middle), $($person.last) (No AUP on file)" -Path ".\logs\staff_$stamp.log"
                }
            } else {
                # Write-Log -Message "Skipping user $($person.id) $($person.emptype) $($person.first), $($person.middle), $($person.last) (No accounts for employee type)" -Path ".\logs\staff_$stamp.log"
            }
        } else {
            $enabled = $true
            #check if terminated
            if ($person.tdate -ne $Null) {
                #terminated in the past
                #set account expiration/deactivate/disable
                $enabled = $false
                #check if rehired
                if ($person.emptype -eq "1S" -or $person.emptype -eq "2SSec" -or ($person.rdate -ne $Null -and $person.rdate -gt $person.tdate)) {
                    #rehired after termination
                    #set account active/enable
                    $enabled = $true
                    # Write-Log -Message "Enable rehired user $($person.id) $($person.emptype) $($person.first), $($person.middle), $($person.last) ($($person.rdate))" -Path ".\logs\staff_$stamp.log"
                } else {
                    $enabled = $false
                    # Write-Log -Message "Disable terminated user $($person.id) $($person.emptype) $($person.first), $($person.middle), $($person.last) ($($person.tdate))" -Path ".\logs\staff_$stamp.log"
                }
            }
            # Write-Output "Update $($person.id) $($person.emptype) $($person.first), $($person.middle), $($person.last)"
            UpdateUser $User $person $enabled
        }
    }
    
    # Write-Output "$($person.id), $($person.emptype), $base, $($person.first), $($person.middle), $($person.last)"
}

$adstaff | Format-Table -Property DistinguishedName, EmployeeID, Enabled
$gstaff | Format-Table