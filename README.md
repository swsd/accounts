# Skyward SIS to AD/G Suite #

These scripts take Skyward Datamining exports and creates Active Directory and G Suite accounts for users.

### How do I get set up? ###

* Install gShell and NTFSSecurity Powershell Modules
* Configure gShell
* Rename `default.config.psd1` to `config.psd1`
* Edit `config.psd1`
* Edit `config_descriptions.ps1`, `config_groups.ps1`, `config_ou.ps1`
* Create Skyward Datamining and scheduled tasks
* Run `auto_staff.ps1` to create and update staff accounts
* Run `auto_students.ps1` to create and update staff accounts
* Run `auto_skyward_emails.ps1` to upload student email addresses back to Skyward