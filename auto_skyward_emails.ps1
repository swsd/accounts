<#
.NOTES
    Name: auto_skyward_emails.ps1 
    Author: Ian Turner
    Requires: PowerShell v2 or higher. 
    Version: 
    1.00   18-February-2016 - Initial Release
.SYNOPSIS
    Pushes school email addresses to Skyward.
.DESCRIPTION
    
.EXAMPLE
    auto_skyward_emails.ps1
.LINK
    None
#>
 
Import-module ActiveDirectory
. .\util\Write-Log.ps1

$Config = Import-LocalizedData -FileName config.psd1

# Get the date we're running on
$stamp = Get-Date -f "yyyyMMdd"
$fileName = "skywardemails_$stamp.csv"
$remotePath = $Config.Skyward.RemoteFiles.SchoolEmails
$localPath = Join-Path -Path $PSScriptRoot -ChildPath "skyward\$filename"

# Send email from no_reply
Function SendMail($to, $subject, $body) {
    $emailSmtpServer = $Config.Email.SMTP.Server
    $emailSmtpServerPort = $Config.Email.SMTP.ServerPort
    $emailSmtpUser = $Config.Email.SMTP.Username
    $emailSmtpPass = $Config.Email.SMTP.Password
     
    $emailMessage = New-Object System.Net.Mail.MailMessage
    $emailMessage.From = $Config.Email.From
    foreach ($add in $to) {
        $emailMessage.To.Add( $add )
    }
    foreach ($add in $Config.Email.BCC) {
        $emailMessage.Bcc.Add( $add )
    }
    $emailMessage.ReplyTo = $Config.Email.ReplyTo
    $emailMessage.Subject = "$subject"
    $emailMessage.IsBodyHtml = $false
    $emailMessage.Body = "$body"
     
    $SMTPClient = New-Object System.Net.Mail.SmtpClient( $emailSmtpServer , $emailSmtpServerPort )
    $SMTPClient.EnableSsl = $true
    $SMTPClient.Credentials = New-Object System.Net.NetworkCredential( $emailSmtpUser , $emailSmtpPass );
     
    $SMTPClient.Send( $emailMessage )
}

Function Log($message) {
    Write-Log -Message $message -Path (join-path -Path (Convert-Path .) -ChildPath "logs\Skyward_emails_$stamp.log")
}

# Set the properties we want when querying for ADUsers
$ADProperties = "employeeid","emailaddress","employeetype"

# Get all current student accounts
Get-ADUser -Filter * -SearchBase "OU=Students,OU=SWSD,DC=sw,DC=site" -Properties $ADProperties | Where-Object {$_.employeeid -ne $null -and $_.employeetype -ne $null} | Select-Object employeeID,emailaddress,employeeType | Export-Csv $localPath -notype

try
{
    # Load WinSCP .NET assembly
    Add-Type -Path ".\util\WinSCPnet.dll"

    # Setup session options
    $sessionOptions = New-Object WinSCP.SessionOptions
    $sessionOptions.Protocol = [WinSCP.Protocol]::Sftp
    $sessionOptions.HostName = $Config.Skyward.HostName
    $sessionOptions.UserName = $Config.Skyward.UserName
    $sessionOptions.Password = $Config.Skyward.Password
    $sessionOptions.SshHostKeyFingerprint = $Config.Skyward.SshHostKeyFingerprint

    $session = New-Object WinSCP.Session

    try
    {
        # Connect
        $session.Open($sessionOptions)

        # if ($session.FileExists($remotePath))
        if ((Test-Path $localPath))
        {
            # if ($session.FileExists($remotePath))
            # {
                # Write-Log -Message (
                    # "File {0} exists, remote {1} does not" -f
                    # $localPath, $remotePath)
                # $upload = $True
            # }

            # if ($upload)
            # {
                # Upload the file and throw on any error
                $session.PutFiles($localPath, $remotePath).Check()

                Log "Upload done."
            # }
        }
        else
        {
            Log ("File {0} does not exist yet" -f $localPath)
        }
    }
    finally
    {
        # Disconnect, clean up
        $session.Dispose()
    }
}
catch [Exception]
{
    Log $_.Exception.Message
    exit 1
}

#
