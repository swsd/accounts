function Get-Staff-Group($building_code, $emp_code, $coach_building) {
    if ($emp_code -eq "2S" -or $emp_code -eq "2SP" -or $emp_code -eq "2R") {
        return @()
    } elseif ($emp_code -eq "2S-Lock" -or $emp_code -eq "2SSec") {
        return @("Substitutes Classified")
    } elseif ($emp_code -eq "2SCDL") {
        return @("Substitute Bus Drivers")
    } elseif ($emp_code -eq "1S") {
        return @("Substitutes Certificated")
    } elseif ($emp_code -eq "COB" -or $emp_code -eq "2CD") {
        return $Null
    } elseif ($building_code -eq "400") {
        if ($emp_code -eq "1MT") {
            return @("HS Staff", "HS Teachers")
        } else {
            return @("HS Staff")
        }
    } elseif ($building_code -eq "200") {
        if ($emp_code -eq "1MT") {
            return @("LMS Staff", "MS Teachers")
        } else {
            return @("LMS Staff")
        }
    } elseif ($building_code -eq "100") {
        if ($emp_code -eq "1MT") {
            return @("Elem Staff", "ES Teachers")
        } else {
            return @("Elem Staff")
        }
    } elseif ($building_code -eq "700") {
        if ($emp_code -eq "1MT") {
            return @("SWA Staff", "SWA Teachers")
        } else {
            return @("SWA Staff")
        }
    } elseif ($building_code -eq "800") {
        return @("TM Staff")
    } elseif ($building_code -eq "900") {
        return @("DO Staff")
    } elseif ($building_code -eq "12") {
        return @("Substitutes Certificated")
    } elseif ($building_code -eq "08") {
        return @("Tech Staff")
    } elseif ($emp_code -eq "2C") {
        if ($coach_building -eq "400") {
            return "HS Staff","Coaches"
        } elseif ($coach_building -eq "200") {
            return "LMS Staff","Coaches"
        } elseif ($coach_building -eq "100") {
            return "Elem Staff","Coaches"
        } elseif ($coach_building -eq "700") {
            return "SWA Staff","Coaches"
        }
        return @("Coaches")
    # } elseif ($building_code -eq "00") {
        # return @("_Staff")
    }
}

function Get-Staff-GGroup($building_code, $emp_code, $coach_building) {
    if ($emp_code -eq "2S" -or $emp_code -eq "2SP" -or $emp_code -eq "2R") {
        return @()
    } elseif ($emp_code -eq "2S-Lock" -or $emp_code -eq "2SSec") {
        return @("substitutes_classified@sw.wednet.edu")
    } elseif ($emp_code -eq "2SCDL") {
        return @("substitute_bus_drivers@sw.wednet.edu")
    } elseif ($emp_code -eq "1S") {
        return @("substitutes_certificated@sw.wednet.edu")
    } elseif ($emp_code -eq "COB" -or $emp_code -eq "2CD") {
        return $Null
    } elseif ($building_code -eq "400") {
        if ($emp_code -eq "1MT") {
            return @("hs_staff@sw.wednet.edu","hs_teachers@sw.wednet.edu")
        } else {
            return @("hs_staff@sw.wednet.edu")
        }
    } elseif ($building_code -eq "200") {
        if ($emp_code -eq "1MT") {
            return @("ms-7-8-staff@sw.wednet.edu","ms_teachers@sw.wednet.edu")
        } else {
            return @("ms-7-8-staff@sw.wednet.edu")
        }
    } elseif ($building_code -eq "100") {
        if ($emp_code -eq "1MT") {
            return @("elem_staff@sw.wednet.edu","es_teachers@sw.wednet.edu")
        } else {
            return @("elem_staff@sw.wednet.edu")
        }
    } elseif ($building_code -eq "700") {
        if ($emp_code -eq "1MT") {
            return @("swastaff@sw.wednet.edu","swa_teachers@sw.wednet.edu")
        } else {
            return @("swastaff@sw.wednet.edu")
        }
    } elseif ($building_code -eq "800") {
        return @("tm_staff@sw.wednet.edu")
    } elseif ($building_code -eq "900") {
        return @("do_staff@sw.wednet.edu")
    } elseif ($building_code -eq "12") {
        return @("substitutes_certificated@sw.wednet.edu")
    } elseif ($building_code -eq "08") {
        return @("tech-staff@sw.wednet.edu")
    } elseif ($emp_code -eq "2C") {
        if ($coach_building -eq "400") {
            return @("hs_staff@sw.wednet.edu","coaches.group@sw.wednet.edu")
        } elseif ($coach_building -eq "200") {
            return "lms_staff@sw.wednet.edu","coaches.group@sw.wednet.edu"
        } elseif ($coach_building -eq "100") {
            return "elem_staff@sw.wednet.edu","coaches.group@sw.wednet.edu"
        } elseif ($coach_building -eq "700") {
            return "swastaff@sw.wednet.edu","coaches.group@sw.wednet.edu"
        }
        return @("coaches.group@sw.wednet.edu")
    }
}